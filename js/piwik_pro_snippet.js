(function (window, document, drupalSettings) {
  const dataLayerName = drupalSettings.piwik_pro.data_layer;
  const siteId = drupalSettings.piwik_pro.site_id;
  const piwikDomain = drupalSettings.piwik_pro.piwik_domain;

  function stgCreateCookie(a, b, c) {
    let d = '';
    if (c) {
      const e = new Date();
      e.setTime(e.getTime() + 24 * c * 60 * 60 * 1e3);
      d = `; expires=${e.toUTCString()}`;
    }
    document.cookie = `${a}=${b}${d}; path=/`;
  }

  if (dataLayerName && siteId && piwikDomain) {
    window[dataLayerName] = window[dataLayerName] || [];
    window[dataLayerName].push({
      start: new Date().getTime(),
      event: 'stg.start',
    });

    const scripts = document.getElementsByTagName('script')[0];
    const tags = document.createElement('script');

    const isStgDebug =
      (window.location.href.match('stg_debug') ||
        document.cookie.match('stg_debug')) &&
      !window.location.href.match('stg_disable_debug');
    stgCreateCookie('stg_debug', isStgDebug ? 1 : '', isStgDebug ? 14 : -1);

    const qP = [];
    if (dataLayerName !== 'dataLayer') {
      qP.push(`data_layer_name=${dataLayerName}`);
    }
    if (isStgDebug) {
      qP.push('stg_debug');
    }
    const qPString = qP.length > 0 ? `?${qP.join('&')}` : '';

    tags.async = true;
    tags.src = `${piwikDomain}${siteId}.js${qPString}`;
    scripts.parentNode.insertBefore(tags, scripts);

    (function (a, n, i) {
      a[n] = a[n] || {};
      for (let c = 0; c < i.length; c++) {
        (function (i) {
          a[n][i] = a[n][i] || {};
          a[n][i].api =
            a[n][i].api ||
            function (...args) {
              if (typeof args[0] === 'string') {
                window[dataLayerName].push({
                  event: `${n}.${i}:${args[0]}`,
                  parameters: args.slice(1),
                });
              }
            };
        })(i[c]);
      }
    })(window, 'ppms', ['tm', 'cm']);
  }
})(window, document, drupalSettings);
